vdjtools (1.2.1+git20190311+repack-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.2.1+git20190311+repack:
    - Repacking to remove the gradle directory and the gradlew script
  * Using javahelper for the build
  * Setting the classpath of the built jar
  * Revising the list of (build-)dependencies
  * Setting the Section of the -java package to non-free/java
  * Adding a Comment in d/copyright to explain why the package lies in non-free
  * Simplifying d/maven.rules
  * Removing unneeded files from the debian/ directory
  * Adding a header to the patch
  * Setting /bin/sh in the shebang of our provided shell script
  * Updating the list of dependencies in the patch
  * Using maven_repo_helper to provide Maven artifacts
  * Removing the flatDir directory in build.gradle to build with a proper
    classpath
  * Preventing gradle from shipping a fat jar with bundled dependencies

  [ Andreas Tille ]
  * Fix watch file
  * Standards-Version: 4.6.1 (routine-update)

 -- Pierre Gruet <pgt@debian.org>  Sun, 04 Dec 2022 07:17:24 +0100

vdjtools (1.2.1+git20190311-6) unstable; urgency=medium

  * Team upload.
  * Fix watchfile to detect new versions on github
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Wed, 06 Oct 2021 17:31:53 +0200

vdjtools (1.2.1+git20190311-5) unstable; urgency=high

  * Team upload
  * Reduce precision of test result to work on all architectures
    Closes: #982116

 -- Andreas Tille <tille@debian.org>  Tue, 09 Feb 2021 19:57:29 +0100

vdjtools (1.2.1+git20190311-4) unstable; urgency=medium

  * Team upload.
  * cme fix dpkg-control
  * Add missing Depends to provide java command
    Closes: #979760
  * Add salsa-ci file (routine-update)
  * No tab in license text (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Wed, 13 Jan 2021 11:35:59 +0100

vdjtools (1.2.1+git20190311-3) UNRELEASED; urgency=medium

  * Transfer vdjtools from Java to Debian Med team

 -- tony mancill <tmancill@debian.org>  Sat, 05 Dec 2020 09:45:34 -0800

vdjtools (1.2.1+git20190311-2) unstable; urgency=medium

  * Team upload.
  * Install JAR symlink in libvdjtools-java package (Closes: #946492)
  * Configure autopkgtest (Closes: #945612)
  * Correct Homepage link
  * Freshen debian/copyright
  * Rework missing library patch for trove and enable tests
  * Update to debhelper compat level 13
  * Bump Standards-Version to 4.5.1
  * Set Rules-Requires-Root: no in debian/control

 -- tony mancill <tmancill@debian.org>  Fri, 04 Dec 2020 12:33:38 -0800

vdjtools (1.2.1+git20190311-1) unstable; urgency=medium

  * Initial release (Closes: #903207)

    Upstream accepted my patch to extend the gradle search path
    for jars.

    Added new patch to find the commons-math3 library.
    https://github.com/mikessh/vdjtools/pull/128/commits/177014136a716416b3b697ce31df365472066225

 -- Steffen Moeller <moeller@debian.org>  Wed, 14 Aug 2019 17:38:26 +0200

vdjtools (1.2.1-1) unstable; urgency=medium

  * Second attempt.

  * Contributed tiny 'vdjtools' shell script to wrap invocation of jar
  * Creating link from /usr/share/java/vdjtools-$VERSION.jar to
    /usr/share/java/vdjtools.jar

 -- Steffen Moeller <moeller@debian.org>  Mon, 14 Jan 2019 12:14:12 +0100

vdjtools (1.1.10-1) UNRELEASED; urgency=medium

  * Initial attempt.

    This is the last release built with maven, upstream recently released
    a switch to gradle. FTBFS because of missing groovy-eclipse-compiler.

 -- Steffen Moeller <moeller@debian.org>  Fri, 06 Jul 2018 14:16:14 +0200
